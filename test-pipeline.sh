#!/bin/bash
set -e
export PATH="$PATH:/Users/ruchaparab/development/flutter/bin"
export PATH="$PATH":"$HOME/.pub-cache/bin"
export PATH="$PATH":"/Users/ruchaparab/development/flutter/bin/cache/dart-sdk/bin"
cd /Users/ruchaparab/Office/cashoo-flutter
flutter pub get
flutter pub global activate junitreport
rm -rf tests-results
mkdir tests-results
flutter test --machine | tojunit --output tests-results/TEST-report.xml
